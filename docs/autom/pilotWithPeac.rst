..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2021 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.



#####################################################################################
Pilotage de l’exécution de tests automatisés via un PEAC (Plan d’Exécution «as code»)
#####################################################################################

.. contents::
   :local:
   :depth: 1

|

|squashAutom| permet la rédaction de plans d’exécution dans un formalisme spécifique à |squashOrchestrator|, les PEAC
(Plan d’Exécution «as code»), pour orchestrer précisément l’exécution des tests automatisés en dehors d’un référentiel
de test.

Retrouvez plus d’informations sur la rédaction d’un PEAC au sein de la documentation de |squashOrchestrator|
(*Squash Orchestrator Documentation – 1.0.0.RELEASE* version ``.pdf``) accessible depuis
https://www.squashtest.com/community-download.
