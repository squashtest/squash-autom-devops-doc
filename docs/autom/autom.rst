..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2021 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

.. _autom:

############
Squash AUTOM
############

.. toctree::
   :hidden:
   :maxdepth: 2

   Installation<install.rst>
   Pilotage d’exécution via un PEAC<pilotWithPeac.rst>
   Pilotage d’exécution depuis Squash TM<pilotFromSquash.rst>

Ce guide vous présente les possibilités offertes par la version *1.1.0.RELEASE* de |squashAutom|.


Cette version *1.1.0.RELEASE* met à votre disposition les composants suivants |_| :

* |squashOrchestrator| : il s’agit d’un outil composé d’un ensemble de micro-services exploitables via l’envoi
  d’un plan d’exécution sous un formalisme bien précis, le PEAC (Plan d’Exécution «as code»), afin d’orchestrer
  des exécutions de tests automatisés.

..

* **Agent OpenTestFactory** : cet agent permet des communications via le protocole HTTP
  entre un |squashOrchestrator| et un environnement d'exécution de tests. Il est actuellement nécessaire pour l'exécution
  de tests Agilitest, Ranorex ou UFT.

..

* **Plugin Result Publisher pour Squash TM** : ce plugin pour |squashTM| permet la remontée d’informations vers
  |squashTM| en fin d’exécution d’un plan d’exécution |squashTM| par l’orchestrateur Squash.

..

* **Plugin Squash AUTOM pour Squash TM** : ce plugin pour |squashTM| permet d'exécuter des tests automatisés
  depuis ce dernier via |squashOrchestrator|.

|
