..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2021 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.



####################
Guide d'Installation
####################

.. contents::
   :local:
   :depth: 1

|

.. _squash_orchestrator_installation:

*******************
Squash Orchestrator
*******************

Présentation
============

|squashOrchestrator| vous permet de diriger et coordonner les différents composants de la chaîne d'exécution de vos tests automatisés
(environnements d'exécution, automates, reporting...).
Il est basé sur l'OpenTestFactory tout en ajoutant un ensemble de micro-services pour étendre ses possibilités,
notamment le fait d'exploiter des plans d'exécutions Squash TM ou d'effectuer du reporting vers ce dernier.

Installation
============

|squashOrchestrator| se présente sous la forme d'un ensemble de services fonctionnant ensemble. Ils peuvent ou non être
lancés sur une même machine, et être ou non démarrés simultanément.

Le seul prérequis est que le service *EventBus*, qui permet la communication entre les différents micro-services, soit disponible
au moment du lancement des autres micro-services.

Pour faciliter l'installation de |squashOrchestrator|, une image docker "all-in-one" est mise à disposition. Elle contient
l'ensemble des services nécessaires de l'OpenTestFactory et une partie des services spécifiques Squash.

Pour récupérer la dernière version de l'image de |squashOrchestrator|, la commande suivante est à exécuter :

.. code-block:: bash

 docker pull squashtest/squash-orchestrator:latest

Usage
=====

Configuration de l'image
------------------------

La commande suivante permet de démarrer |squashOrchestrator| en lui permettant d'utiliser un environnement d'exécution existant,
avec auto-génération des clés de trusted keys (ce qui n'est **pas** recommandé dans un environnement de production).

.. code-block:: bash

  docker run -d \
           --name orchestrator \
           -p 7774:7774 \
           -p 7775:7775 \
           -p 7776:7776 \
           -p 38368:38368 \
           -e SSH_CHANNEL_HOST=the_environment_ip_or_hostname \
           -e SSH_CHANNEL_USER=user \
           -e SSH_CHANNEL_PASSWORD=secret \
           -e SSH_CHANNEL_TAGS=ssh,linux,robotframework \
            squashtest/squash-orchestrator:latest

Cette commande expose les services suivants sur les ports correspondants :

* *receptionnist* (port 7774)
* *observer* (port 7775)
* *killswitch* (port 7776)
* *eventbus* (port 38368)

Lancement de l'image en mode Premium
------------------------------------

Si vous disposez d'une licence |squashAutom| **Premium** et avez déployé la version **Premium** du plugin Squash TM *Result Publisher*,
vous devez démarrez |squashOrchestrator| en mode **Premium** pour profitez du comportement **Premium** pour la remontée de résultats vers
|squashTM|. Pour cela, il faut ajouter le paramètre suivant dans la commande de démarrage de |squashOrchestrator| : ``-e SQUASH_LICENCE_TYPE=premium``

Pour une configuration plus complète de |squashOrchestrator|, vous pouvez vous reporter à la `documentation
de l'OpenTestFectory <https://opentestfactory.github.io/orchestrator/installation/>`_ qui sert de base au |squashOrchestrator|.

|

****************************************************************
Image Docker des micro-services exclusifs à Squash AUTOM Premium
****************************************************************

Présentation
============

Posséder une licence Squash AUTOM Premium permet d'avoir accès à une image Docker contenant des micro-services pour
|squashOrchestrator| fournissant les fonctionnalités suivantes :

* Gestion de l'exécution de tests Agilitest
* Gestion de l'exécution de tests Ranorex
* Gestion de l'exécution de tests UFT

Installation
============

Afin d'installer l'image Docker des services exclusifs à Squash AUTOM Premium, vous devez récupérer l'image compressée
auprès du service support Squash puis exécuter la commande suivante :

.. code-block:: bash

 docker load -i squash-autom-premium-2.0.0.tar.gz

Usage
=====

Pour exécuter l'image Docker des services exclusifs à Squash AUTOM Premium, la commande suivante est à exécuter :


.. code-block:: bash

 docker run -e BUS_HOST=<IP ou nom DNS du bus> \
            -e BUS_PORT=<port> -e BUS_TOKEN=<token JWT> \
            --volume /chemin/vers/clef/publiques(s):/etc/squashtf/ \
            docker.squashtest.org/squashtest/squash-autom-premium:2.0.0

avec :

* *BUS_HOST* (obligatoire) : l'adresse IP ou le nom DNS du service *EventBus* du |squashOrchestrator| avec qui les micro-services
  communiquent.
* *BUS_PORT* (obligatoire) : port du service *EventBus* du |squashOrchestrator| avec qui les micro-services
  communiquent.
* *BUS_TOKEN* (facultatif) : token JWT accepté par le service *EventBus* du |squashOrchestrator| avec qui les micro-services
  communiquent.
* *--volume* : volume à monter vers un ensemble de clés publiques acceptées pour la validité de tokens JWT.
  Il est à mettre en place si une validation de token JWT est nécessaire pour l'échange de messages entre micro-services.


|

*********************
Agent OpenTestFactory
*********************

Présentation
============

L'agent OpenTestFactory est un process qui tourne sur un environnement d'exécution. Ce process contacte le |squashOrchestrator|
à intervalle régulier, à la recherche d'ordres à exécuter. S'il y a un ordre en attente, l'agent va l'exécuter
puis retourner le résultat à l'orchestrateur.

Installation
============

L'agent OpenTestFactory est une application Python à installer sur un environnement d'exécution de tests automatisés.
Il requiert Python 3.8 ou supérieure. Il fonctionne sur un environnement Linux, MacOs ou Windows.

L'agent se présente comme un simple script. Il possède seulement une dépendance, vers la librairie python ``requests``
(elle sera installée si elle n'est pas déjà présente sur l'environnement d'exécution).

Afin d'installer l'agent, la commande suivante est à exécuter :

.. code-block:: bash

  pip3 install --upgrade opentf-agent

Vous pouvez vérifier l'installation en exécutant la commande suivante :

.. code-block:: bash

  opentf-agent --help

Usage
=====

Résumé
------

.. code-block:: bash

  $ opentf-agent --help
  usage: opentf-agent [-h] --tags TAGS --host HOST [--port PORT] [--path_prefix PATH_PREFIX] [--token TOKEN] [--encoding ENCODING] [--script_path SCRIPT_PATH]
                      [--workspace_dir WORKSPACE_DIR] [--name NAME] [--polling_delay POLLING_DELAY] [--liveness_probe LIVENESS_PROBE] [--retry RETRY] [--debug]

  OpenTestFactory Agent

  optional arguments:
    -h, --help            show this help message and exit
    --tags TAGS           a comma-separated list of tags (e.g. windows,robotframework)
    --host HOST           target host with protocol (e.g. https://example.local)
    --port PORT           target port (default to 24368)
    --path_prefix PATH_PREFIX
                        target context path (default to no context path)
    --token TOKEN         token
    --encoding ENCODING   encoding on the console side (defaults to utf-8)
    --script_path SCRIPT_PATH
                        where to put temporary files (defaults to current directory)
    --workspace_dir WORKSPACE_DIR
                        where to put workspaces (defaults to current directory)
    --name NAME           agent name (defaults to "test agent")
    --polling_delay POLLING_DELAY
                        polling delay in seconds (default to 5)
    --liveness_probe LIVENESS_PROBE
                        liveness probe in seconds (default to 300 seconds)
    --retry RETRY         how many time to try joining host (default to 5,
                        0 = try forever)
    --debug               whether to log debug informations.

Exemple
-------

En considérant qu'un |squashOrchestrator| est lancé sur ``orchestrator.example.com``, avec un jeton stocké
dans la variable d'environnement ``TOKEN``, la commande suivante enregistre l'environnement d'exécution Windows et recevra
les commandes ciblant windows et/ou robotframework :

.. code-block:: bash

  chcp 65001
  opentf-agent --tags windows,robotframework --host https://orchestrator.example.com/ --token %TOKEN%

L'agent contactera l'orchestrateur toutes les 5 secondes, et exécutera les commandes receptionnées.
La commande ``chcp`` configure la console en Unicode. Il s'agit d'une spécificité Windows. Cette configuration peut être nécessaire
suivant le framework de test disponible sur l'environnement d'exécution.

|

**************************************
Plugin Result Publisher pour Squash TM
**************************************

Présentation
============

Ce plugin est à utiliser conjointement avec |squashOrchestrator| pour permettre la remontée de résultats à |squashTM|
à la fin d'une exécution de tests automatisés d'un plan d'exécution |squashTM|.

Le plugin existe en version **Community** (*squash.tm.rest.result.publisher.community-1.0.0.RELEASE.jar*) librement
téléchargeable ou **Premium** (*squash.tm.rest.result.publisher.premium-1.0.0.RELEASE.jar*) accessible sur demande.

Installation
============

Pour l’installation, merci de vous reporter au protocole d’installation d’un plugin |squashTM|
(https://sites.google.com/a/henix.fr/wiki-squash-tm/installation-and-exploitation-guide/2---installation-of-squash-tm/7---jira-plug-in).

.. warning:: Ce plugin est compatible avec une version *1.22.2.RELEASE* ou supérieure de |squashTM|.

|


**************************************
Plugin Squash AUTOM pour Squash TM
**************************************

Présentation
============

Ce plugin est à utiliser conjointement avec |squashOrchestrator| pour permettre l'exécution de tests automatisés via |squashOrchestrator|
depuis |squashTM|.

Le plugin existe en version **Community** (*plugin.testautomation.squashautom.community-1.0.0.RELEASE.jar*) librement
téléchargeable ou **Premium** (*plugin.testautomation.squashautom.premium-1.0.0.RELEASE.jar*) accessible sur demande.

Installation
============

Pour l’installation, merci de vous reporter au protocole d’installation d’un plugin |squashTM|
(https://sites.google.com/a/henix.fr/wiki-squash-tm/installation-and-exploitation-guide/2---installation-of-squash-tm/7---jira-plug-in).

.. warning:: Ce plugin est compatible avec une version *1.22.2.RELEASE* ou supérieure de |squashTM|.
