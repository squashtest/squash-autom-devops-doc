..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2021 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

#############################################################
Pilotage de l’exécution de tests automatisés depuis Squash TM
#############################################################

.. contents::
   :local:
   :depth: 1

.. note:: Afin de piloter l'exécution de tests automatisés depuis Squash TM, vous avez besoin des composants suivants :

          * Squash TM
          * Squash Orchestrator
          * Plugin Result Publisher pour Squash TM
          * Plugin Squash AUTOM pour Squash TM
          * Si exécution de cas de test *Agilitest* : l'agent *OpenTestFactory* pour l'environnement d'exécution
            des tests *Agilitest*
          * Si exécution de cas de test *Ranorex* : l'agent *OpenTestFactory* pour l'environnement d'exécution
            des tests *Ranorex*, et le chemin vers le dossier contenant *MSBuild.exe* renseigné dans une variable d'environnement
            nommée *SQUASH_MSBUILD_PATH*
          * Si exécution de cas de test *UFT* : l'agent *OpenTestFactory* pour l'environnement d'exécution
            des tests *UFT*

|

.. _automate_with_squash:

*****************************************
Automatisation d’un cas de test Squash TM
*****************************************

.. note:: Cette page décrit les opérations communes à tous les frameworks de test supportés par cette version.
          Pour faciliter la navigation vous pouvez directement consulter les spécificités de l'automatisation de chaque
          technologie grâce aux liens suivants |_| :

          * :ref:`Agilitest<automate_with_agilitest>`
          * :ref:`Cucumber<automate_with_cucumber>`
          * :ref:`Cypress<automate_with_cypress>`
          * :ref:`JUnit<automate_with_junit>`
          * :ref:`Ranorex<automate_with_ranorex>`
          * :ref:`Robot Framework<automate_with_robot>`
          * :ref:`SKF<automate_with_skf>`
          * :ref:`SoapUI<automate_with_soapui>`
          * :ref:`UFT<automate_with_uft>`

Sans utilisation de workflow d’automatisation Squash
====================================================

Pour qu’un cas de test soit utilisable par |squashOrchestrator|, il faut que le panneau *Automatisation* de l’onglet
*Information* de la page de consultation d’un cas de test soit correctement renseigné |_| :

    .. container:: image-container

        .. image:: ../_static/autom/tc-automated-space.png

* ``Technologie du test automatisé`` : Liste déroulante permettant de choisir la technologie utilisée pour exécuter le
  cas de test. Dans cette version, seuls les choix *Robot Framework*, *Junit*, *Cucumber*, *Cypress*, *SKF*, *SoapUi*,
  *Agilitest*, *Ranorex* et *UFT* sont fonctionnels.

* ``URL du dépôt de code source`` : L’adresse du dépôt de source où se trouve le projet, tel que spécifié dans l'espace
  *Serveurs de partage de code source* de l’*Administration*.

* ``Référence du test automatisé`` : Correspond à l’emplacement du test automatisé dans le projet. Cette référence doit
  respecter un format propre à la technologie de test employée (voir :ref:`ici<automation_specifics>`).

.. note:: *Agilitest*, *Ranorex* et *UFT* ne sont supportés que par la version *premium* de |squashAutom|.

--------------------

Avec utilisation de workflow d’automatisation Squash
====================================================

Cas de test classique
---------------------

Pour qu’un cas de test soit utilisable par |squashOrchestrator|, il doit être automatisé à partir de l’espace
*Automatisation (Automaticien)* via trois colonnes à renseigner |_| :

    .. container:: image-container

        .. image:: ../_static/autom/autom-workspace-columns.png

* ``Tech. test auto.`` : Liste déroulante permettant de choisir la technologie utilisée pour exécuter le cas de test. Dans
  cette version, seuls les choix *Robot Framework*, *Junit*, *Cucumber*, *Cypress*, *SKF*, *SoapUi*, *Agilitest*, *Ranorex*
  et *UFT* sont fonctionnels.

* ``URL du SCM`` : L’adresse du dépôt de source où se trouve le projet.

* ``Ref. test auto.`` : Correspond à l’emplacement du test automatisé dans le projet. Cette référence doit respecter un
  format propre à la technologie de test employée (voir :ref:`ici<automation_specifics>`).

.. note:: *Agilitest*, *Ranorex* et *UFT* ne sont supportés que par la version *premium* de |squashAutom|.

Cas de test BDD ou Gherkin
--------------------------

Les informations du panneau *Automatisation* sont remplis automatiquement lors de la transmission d’un script BDD ou
Gherkin à un gestionnaire de code source distant. Ils sont également modifiables à tout moment par l’utilisateur.

--------------------

.. _squash_params:

Exploitation de paramètres Squash TM
====================================

Au lancement d’un plan d’exécution |squashTM| (via un PEAC ou directement depuis l'espace campagne), celui-ci transmet
différentes informations sur les ITPI qu’il est possible d’exploiter dans un cas de test *Cucumber*, *Cypress*,
*Robot Framework*, *SKF*, *Agilitest*, *Ranorex* ou *UFT*. Les détails de cette fonctionnalité sont décrits dans la section
correspondant à la technologie utilisée.

--------------------

.. _automation_specifics:

Spécificités pour l’automatisation suivant le framework d’automatisation
========================================================================

.. _automate_with_agilitest:

Automatisation avec Agilitest
-----------------------------

1. Référence du test
^^^^^^^^^^^^^^^^^^^^

Pour lier un cas de test |squashTM| à un test automatisé *Agilitest*, le champ *Référence* du test automatisé du bloc
*Automatisation* du cas de test doit avoir la forme suivante |_| :

``[1] / [2]``

Avec :

* ``[1]`` : Nom du projet sur le dépôt de source.

* ``[2]`` : Chemin et nom du fichier de script *ActionTestScript* à partir de la racine du projet (avec son extension ``.ats``).

.. warning:: Les scripts ATS doivent impérativement se trouver dans une arborescence de fichiers de la forme
             ``src/main/ats/*`` , conformément à l'architecture classique d'un projet ATS.

2. Nature des paramètres Squash TM exploitables
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Les paramètres |squashTM| exploitables dans un script *ActionTestScript* vont différer suivant si vous utilisez les
composants **Squash DEVOPS Community** ou **Squash DEVOPS Premium**.

Voici le tableau des paramètres exploitables |_| :

.. csv-table::
   :header: "Nature","Clé","Community","Premium"
   :align: center

   "Nom du jeu de donnée","DSNAME",.. image:: ../_static/images/ok.jpg,.. image:: ../_static/images/ok.jpg
   "Paramètre d’un jeu de donnée","DS_[nom]",.. image:: ../_static/images/ok.jpg,.. image:: ../_static/images/ok.jpg
   "Référence du cas de test","TC_REF",.. image:: ../_static/images/ok.jpg,.. image:: ../_static/images/ok.jpg
   "CUF cas de test","TC_CUF_[code]",.. image:: ../_static/images/ok.jpg,.. image:: ../_static/images/ok.jpg
   "CUF itération","IT_CUF_[code]",.. image:: ../_static/images/ko.jpg,.. image:: ../_static/images/ok.jpg
   "CUF campagne","CPG_CUF_[code]",.. image:: ../_static/images/ko.jpg,.. image:: ../_static/images/ok.jpg
   "CUF suite de tests","TS_CUF_[code]",.. image:: ../_static/images/ko.jpg,.. image:: ../_static/images/ok.jpg

*Légende :*

* ``CUF`` : *Custom Field / Champ personnalisé*
* ``[code]`` : *Valeur renseignée dans le champ “Code” d’un CUF*
* ``[nom]`` : *nom du paramètre tel que renseigné dans Squash TM*

3. Utilisation de paramètres Squash TM
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Il est possible lors de l’exécution d’un cas de test |squashTM| automatisé avec *Agilitest* d’exploiter des
paramètres |squashTM| au sein de celui-ci.

Pour cela, il faut suivre les étapes suivantes |_| :

* Renseigner des champs personnalisés dans |squashTM| et les associer au projet portant le plan de tests à exécuter.

* S'assurer que les champs *code* des paramètres correspondent aux noms des variables d'environnement existants dans le
  script *ActionTestScript*.

.. note:: |squashTM| ajoute un préfixe au *code* du champ personnalisé transmis. Assurez-vous de le prendre en compte.
          Voir la `documentation <https://sites.google.com/a/henix.fr/wiki-squash-tm/administrator-guide/05---custom-field-administration>`_
          de |squashTM| pour plus d'information.

|

Ci-dessous un exemple d'un fichier de test *Agilitest* et l’automatisation du cas de test |squashTM| associé |_| :

    .. container:: image-container

        .. image:: ../_static/autom/agilitest-test-with-params-1.png

    .. container:: image-container

        .. image:: ../_static/autom/agilitest-test-with-params-2.png

|

--------------------

.. _automate_with_cucumber:

Automatisation avec Cucumber
----------------------------

1. Référence du test
^^^^^^^^^^^^^^^^^^^^

.. note:: Dans le cas où un nom de scénario n'est pas spécifié, le résultat de chaque exécution du cas de test |squashTM| est calculé en prenant en compte les
          résultats individuels de chaque scénario inclus dans le fichier lié |_| :

          * Si au moins un scénario est en statut *Error* (dans le cas d'un problème technique), l'exécution sera en
            statut *Blocked*.
          * Si au moins un scénario a échoué fonctionnellement et qu'aucun scénario n'est en statut *Error*, l'exécution
            sera en statut *Failed*.
          * Si tous les scénarios ont réussi, l'exécution sera en statut *Success*.

Pour lier un cas de test |squashTM| à un test automatisé *Cucumber*, le champ *Référence* du test automatisé du bloc
*Automatisation* du cas de test doit avoir la forme suivante |_| :

``[1] / [2] # [3] # [4]``

Avec :

* ``[1]`` : Nom du projet sur le dépôt de source.

* ``[2]`` : Chemin et nom du fichier de test *Cucumber* à partir de la racine du projet (avec son extension ``.feature``).

* ``[3]`` : Nom de la feature tel que renseigné dans le fichier de test *Cucumber*.

* ``[4]`` : Nom du scénario tel que renseigné dans le fichier de test *Cucumber*.

2. Nature des paramètres Squash TM exploitables
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**Squash AUTOM** et **Squash DEVOPS** sont capables d'exploiter le nom d'un jeu de données Squash TM d'un cas de test
comme valeur d'un tag à utiliser pour l'exécution d'un sous-ensemble spécifique d'une feature *Cucumber*.

Cette exploitation est possible par les composants en version **Community** ou **Premium**.


3. Utilisation de paramètres Squash TM
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Il est possible lors de l’exécution d’un cas de test |squashTM| automatisé avec *Cucumber* d’exploiter un nom
de jeu de données |squashTM| pour n'exécuter qu'un jeu de données particulier d'un scénario *Cucumber*.

Pour cela, il faut suivre les étapes suivantes |_| :

* Renseigner des jeux de données dans l'onglet *Paramètres* du cas de test dans |squashTM|.

* Créer au sein de son scénario *Cucumber* autant de tableaux exemple que de jeux de données et annoter ces tableaux
  d'un tag correspondant au nom d'un jeu de données |squashTM|.

* Créer une seule ligne d'éléments dans chaque tableau exemple afin de fixer une valeur
  pour les différents paramètres du scénario.

|

Ci-dessous un exemple d'un fichier de test *Cucumber* et l’automatisation du cas de test |squashTM| associé |_| :

    .. container:: image-container

        .. image:: ../_static/autom/cucumber-test-with-tags-1.png

    .. container:: image-container

        .. image:: ../_static/autom/cucumber-test-with-tags-2.png

    .. container:: image-container

        .. image:: ../_static/autom/cucumber-test-with-tags-3.png

|

--------------------

.. _automate_with_cypress:

Automatisation avec Cypress
---------------------------

1. Référence du test
^^^^^^^^^^^^^^^^^^^^

.. note:: Dans cette version de |squashAutom| il n'est pas possible de sélectionner un test spécifique dans
          un fichier ``.spec.js`` qui en contiendrait plusieurs |_| : tous les tests du fichier sont donc exécutés ensemble.
          Le résultat de chaque exécution du cas de test |squashTM| est calculé en prenant en compte les
          résultats individuels de chaque test inclus dans le fichier lié |_| :

          * Si au moins un test est en statut *Error* (dans le cas d'un problème technique), l'exécution sera en statut
            *Blocked*.
          * Si au moins un test a échoué fonctionnelement et qu'aucun test n'est en statut *Error*, l'exécution sera en
            statut *Failed*.
          * Si tous les tests ont réussi, l'exécution sera en statut *Success*.

Pour lier un cas de test |squashTM| à un test automatisé *Cypress*, le champ *Référence* du test automatisé du bloc
*Automatisation* du cas de test doit avoir la forme suivante |_| :

``[1] / [2]``

Avec :

* ``[1]`` : Nom du projet sur le dépôt de source.

* ``[2]`` : Chemin et nom du fichier de test *Cypress* à partir de la racine du projet (avec son extension ``.spec.js``).

2. Nature des paramètres Squash TM exploitables
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Les paramètres |squashTM| exploitables dans un script *Cypress* vont différer suivant si vous utilisez les
composants **Squash DEVOPS Community** ou **Squash DEVOPS Premium**.

Voici le tableau des paramètres exploitables |_| :

.. csv-table::
   :header: "Nature","Clé","Community","Premium"
   :align: center

   "Nom du jeu de donnée","DSNAME",.. image:: ../_static/images/ok.jpg,.. image:: ../_static/images/ok.jpg
   "Paramètre d’un jeu de donnée","DS_[nom]",.. image:: ../_static/images/ok.jpg,.. image:: ../_static/images/ok.jpg
   "Référence du cas de test","TC_REF",.. image:: ../_static/images/ok.jpg,.. image:: ../_static/images/ok.jpg
   "CUF cas de test","TC_CUF_[code]",.. image:: ../_static/images/ok.jpg,.. image:: ../_static/images/ok.jpg
   "CUF itération","IT_CUF_[code]",.. image:: ../_static/images/ko.jpg,.. image:: ../_static/images/ok.jpg
   "CUF campagne","CPG_CUF_[code]",.. image:: ../_static/images/ko.jpg,.. image:: ../_static/images/ok.jpg
   "CUF suite de tests","TS_CUF_[code]",.. image:: ../_static/images/ko.jpg,.. image:: ../_static/images/ok.jpg

*Légende :*

* ``CUF`` : *Custom Field / Champ personnalisé*
* ``[code]`` : *Valeur renseignée dans le champ “Code” d’un CUF*
* ``[nom]`` : *nom du paramètre tel que renseigné dans Squash TM*

3. Utilisation de paramètres Squash TM
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Il est possible lors de l’exécution d’un cas de test |squashTM| automatisé avec *Cypress* d’exploiter des
paramètres |squashTM| au sein de celui-ci.

Pour cela, il faut suivre les étapes suivantes |_| :

* Renseigner des champs personnalisés dans |squashTM| et les associer au projet portant le plan de tests à exécuter.

* S'assurer que les champs *code* des paramètres correspondent aux noms des variables d'environnement existants dans le
  script *Cypress*.

.. note:: |squashTM| ajoute un préfixe au *code* du champ personnalisé transmis. Assurez-vous de le prendre en compte.
          Voir la `documentation <https://sites.google.com/a/henix.fr/wiki-squash-tm/administrator-guide/05---custom-field-administration>`_
          de |squashTM| pour plus d'information.

|

Ci-dessous un exemple d'un fichier de test *Cypress* et l’automatisation du cas de test |squashTM| associé |_| :

    .. container:: image-container

        .. image:: ../_static/autom/cypress-test-with-params-1.png

    .. container:: image-container

        .. image:: ../_static/autom/cypress-test-with-params-2.png

|

--------------------

.. _automate_with_junit:

Automatisation avec JUnit
-------------------------

Référence du test
^^^^^^^^^^^^^^^^^

Pour lier un cas de test |squashTM| à un test automatisé *JUnit*, le champ *Référence* du test automatisé du bloc
*Automatisation* du cas de test doit avoir la forme suivante |_| :

``[1] / [2] # [3]``

Avec :

* ``[1]`` : Nom du projet sur le dépôt de source.

* ``[2]`` : Nom qualifié de la classe de test.

* ``[3]`` : Nom de la méthode à tester dans la classe de test.

|

Ci-dessous un exemple de classe de test *Junit* et l’automatisation du cas de test |squashTM| associé |_| :

    .. container:: image-container

        .. image:: ../_static/autom/junit-test-with-param-1.png

    .. container:: image-container

        .. image:: ../_static/autom/junit-test-with-param-2.png

|

.. _automate_with_ranorex:

--------------------

Automatisation avec Ranorex
---------------------------

Cette fonctionnalité est disponible uniquement dans la version *Premium* de |squashAutom|.

Les prérequis nécessaires pour l'utiliser sont l'installation de l'agent *OpenTestFactory* pour l'environnement d'exécution
des tests *Ranorex*. De plus, le chemin vers le dossier contenant **MSBuild.exe** doit être renseigné dans une variable d'environnement
nommée **SQUASH_MSBUILD_PATH**. Vous pouvez exécuter la commande suivante pour trouver le chemin vers votre exécutable *MSBuild* |_| :

.. code-block:: console

   reg.exe query "HKLM\SOFTWARE\Microsoft\MSBuild\ToolsVersions\4.0" /v MSBuildToolsPath

1. Référence du test
^^^^^^^^^^^^^^^^^^^^

Pour lier un cas de test |squashTM| à un test automatisé, le champ *Référence* du test automatisé du bloc
*Automatisation* du cas de test doit avoir une forme spécifique au framework de test utilisé |_| :

``[1] # [2] # [3] # [4]``

Avec :

* ``[1]`` : Chemin vers le fichier ``.sln`` de la solution à partir de la racine du projet sur le dépôt de source.

* ``[2]`` : Nom du projet *Ranorex* à exécuter.

* ``[3]`` : Nom de la suite de tests *Ranorex* à exécuter.

* ``[4]`` : Nom du cas de test *Ranorex* à exécuter. Ce paramètre est optionnel.

2. Nature des paramètres Squash TM exploitables
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Voici le tableau des paramètres exploitables |_| :

.. csv-table::
   :header: "Nature","Clé"
   :align: center

   "Nom du jeu de donnée","DSNAME"
   "Paramètre d’un jeu de donnée","DS_[nom]"
   "Référence du cas de test","TC_REF"
   "CUF cas de test","TC_CUF_[code]"
   "CUF itération","IT_CUF_[code]"
   "CUF campagne","CPG_CUF_[code]"
   "CUF suite de tests","TS_CUF_[code]"

*Légende :*

* ``CUF`` : *Custom Field / Champ personnalisé*
* ``[code]`` : *Valeur renseignée dans le champ “Code” d’un CUF*
* ``[nom]`` : *nom du paramètre tel que renseigné dans Squash TM*

3. Utilisation de paramètres Squash TM
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Il est possible lors de l’exécution d’un cas de test |squashTM| automatisé avec *Ranorex* d’exploiter des
paramètres |squashTM| au sein de celui-ci.

Pour cela, il faut suivre les étapes suivantes |_| :

* Renseigner des champs personnalisés dans |squashTM| et les associer au projet portant le plan de tests à exécuter.

* S'assurer que les champs *code* des paramètres correspondent aux noms des paramètres existants dans la
  solution *Ranorex*.

.. note:: |squashTM| ajoute un préfixe au *code* du champ personnalisé transmis. Assurez-vous de le prendre en compte.
          Voir la `documentation <https://sites.google.com/a/henix.fr/wiki-squash-tm/administrator-guide/05---custom-field-administration>`_
          de |squashTM| pour plus d'information.

|

Ci-dessous un exemple de solution *Ranorex* et l’automatisation du cas de test |squashTM| associé |_| :

    .. container:: image-container

        .. image:: ../_static/autom/ranorex-test-with-param-1.png

    .. container:: image-container

        .. image:: ../_static/autom/ranorex-test-with-param-2.png

|

.. _automate_with_robot:

--------------------

Automatisation avec Robot Framework
-----------------------------------

1. Référence du test
^^^^^^^^^^^^^^^^^^^^

Pour lier un cas de test |squashTM| à un test automatisé, le champ *Référence* du test automatisé du bloc
*Automatisation* du cas de test doit avoir une forme spécifique au framework de test utilisé |_| :

``[1] / [2] # [3]``

Avec :

* ``[1]`` : Nom du projet sur le dépôt de source.

* ``[2]`` : Chemin et nom du fichier de test *Robot Framework* à partir de la racine du projet (avec son extension ``.robot``).

* ``[3]`` : Nom du cas de test à exécuter dans le fichier ``.robot``.

2. Nature des paramètres Squash TM exploitables
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Les paramètres |squashTM| exploitables dans un script *Robot Framework* vont différer suivant si vous utilisez les
composants **Squash DEVOPS Community** ou **Squash DEVOPS Premium**.

Voici le tableau des paramètres exploitables |_| :

.. csv-table::
   :header: "Nature","Clé","Community","Premium"
   :align: center

   "Nom du jeu de donnée","DSNAME",.. image:: ../_static/images/ok.jpg,.. image:: ../_static/images/ok.jpg
   "Paramètre d’un jeu de donnée","DS_[nom]",.. image:: ../_static/images/ok.jpg,.. image:: ../_static/images/ok.jpg
   "Référence du cas de test","TC_REF",.. image:: ../_static/images/ok.jpg,.. image:: ../_static/images/ok.jpg
   "CUF cas de test","TC_CUF_[code]",.. image:: ../_static/images/ok.jpg,.. image:: ../_static/images/ok.jpg
   "CUF itération","IT_CUF_[code]",.. image:: ../_static/images/ko.jpg,.. image:: ../_static/images/ok.jpg
   "CUF campagne","CPG_CUF_[code]",.. image:: ../_static/images/ko.jpg,.. image:: ../_static/images/ok.jpg
   "CUF suite de tests","TS_CUF_[code]",.. image:: ../_static/images/ko.jpg,.. image:: ../_static/images/ok.jpg

*Légende :*

* ``CUF`` : *Custom Field / Champ personnalisé*
* ``[code]`` : *Valeur renseignée dans le champ “Code” d’un CUF*
* ``[nom]`` : *nom du paramètre tel que renseigné dans Squash TM*

3. Utilisation de paramètres Squash TM
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Il est possible lors de l’exécution d’un cas de test |squashTM| automatisé avec *Robot Framework* d’exploiter des
paramètres |squashTM| au sein de celui-ci.

Pour cela, il faut suivre les étapes suivantes |_| :

* Renseigner des champs personnalisés dans |squashTM| et les associer au projet portant le plan de tests à exécuter.

* Installer sur le/les environnement(s) d’exécution *Robot Framework* la librairie python *squash-tf-services*. Elle est
  accessible par le gestionnaire de package ``pip`` et peut s’installer en exécutant la ligne de commande suivante |_| :

.. code-block:: bash

   python -m pip install squash-tf-services

* Importer la librairie au sein du fichier ``.robot`` dans la section *Settings* |_| :

.. code-block:: bash

    Library squash_tf.TFParamService

* Vous pouvez ensuite récupérer la valeur d’un paramètre |squashTM| en faisant appel au mot-clef suivant |_| :

.. code-block:: bash

    Get Param <clé du paramètre>

|

Ci-dessous un exemple de fichier de test *Robot Framework* et l’automatisation du cas de test |squashTM| associé |_| :

    .. container:: image-container

        .. image:: ../_static/autom/robot-test-with-param-1.png

    .. container:: image-container

        .. image:: ../_static/autom/robot-test-with-param-2.png

|

.. _automate_with_skf:

--------------------

Automatisation avec SKF
---------------------------

1. Référence du test
^^^^^^^^^^^^^^^^^^^^

Pour lier un cas de test |squashTM| à un test automatisé, le champ *Référence* du test automatisé du bloc
*Automatisation* du cas de test doit avoir une forme spécifique au framework de test utilisé |_| :

``[1] / [2] . [3] # [4]``

Avec :

* ``[1]`` : Chemin du dossier *SKF* racine (qui contient le fichier pom.xml) sur le dépôt de source.

* ``[2]`` : Écosystème de tests par défaut du projet *SKF* (tests).

* ``[3]`` : Sous écosytème de tests (il est possible d'en rajouter plusieurs en les séparant par un ``.``; ce paramètre est optionnel).

* ``[4]`` : Nom du script de test à exécuter ( avec son extension ``.ta``; ce paramètre est optionnel, s'il n'est pas renseigné, tous les scripts de tests de l'écosystème cible seront exécutés)

2. Nature des paramètres Squash TM exploitables
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Les paramètres |squashTM| exploitables dans un script *SKF* vont différer suivant si vous utilisez les
composants **Squash DEVOPS Community** ou **Squash DEVOPS Premium**.

Voici le tableau des paramètres exploitables |_| :

.. csv-table::
   :header: "Nature","Clé","Community","Premium"
   :align: center

   "Nom du jeu de donnée","DSNAME",.. image:: ../_static/images/ok.jpg,.. image:: ../_static/images/ok.jpg
   "Paramètre d’un jeu de donnée","DS_[nom]",.. image:: ../_static/images/ok.jpg,.. image:: ../_static/images/ok.jpg
   "Référence du cas de test","TC_REF",.. image:: ../_static/images/ok.jpg,.. image:: ../_static/images/ok.jpg
   "CUF cas de test","TC_CUF_[code]",.. image:: ../_static/images/ok.jpg,.. image:: ../_static/images/ok.jpg
   "CUF itération","IT_CUF_[code]",.. image:: ../_static/images/ko.jpg,.. image:: ../_static/images/ok.jpg
   "CUF campagne","CPG_CUF_[code]",.. image:: ../_static/images/ko.jpg,.. image:: ../_static/images/ok.jpg
   "CUF suite de tests","TS_CUF_[code]",.. image:: ../_static/images/ko.jpg,.. image:: ../_static/images/ok.jpg

*Légende :*

* ``CUF`` : *Custom Field / Champ personnalisé*
* ``[code]`` : *Valeur renseignée dans le champ “Code” d’un CUF*
* ``[nom]`` : *nom du paramètre tel que renseigné dans Squash TM*

3. Utilisation de paramètres Squash TM
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Il est possible lors de l’exécution d’un cas de test |squashTM| automatisé avec *SKF* d’exploiter des
paramètres |squashTM| au sein de celui-ci.

Pour cela, il faut suivre les étapes suivantes |_| :

* Renseigner des champs personnalisés dans |squashTM| et les associer au projet portant le plan de tests à exécuter.

* S'assurer que les champs *code* des paramètres correspondent aux noms des paramètres existants dans le
  projet *SKF*.

.. note:: |squashTM| ajoute un préfixe au *code* du champ personnalisé transmis. Assurez-vous de le prendre en compte.
          Voir la `documentation <https://sites.google.com/a/henix.fr/wiki-squash-tm/administrator-guide/05---custom-field-administration>`_
          de |squashTM| pour plus d'information.

|

Ci-dessous un exemple de projet *SKF* et l’automatisation du cas de test |squashTM| associé |_| :

    .. container:: image-container

        .. image:: ../_static/autom/skf-test-with-params-1.png

    .. container:: image-container

        .. image:: ../_static/autom/skf-test-with-params-2.png

|

.. _automate_with_soapui:

--------------------

Automatisation avec SoapUI
--------------------------

Référence du test
^^^^^^^^^^^^^^^^^

Pour lier un cas de test |squashTM| à un test automatisé *SoapUI*, le champ *Référence* du test automatisé du bloc
*Automatisation* du cas de test doit avoir la forme suivante |_| :

``[1] / [2] # [3] # [4]``

Avec :

* ``[1]`` : Nom du projet sur le dépôt de source.

* ``[2]`` : Chemin et nom du fichier de test *SoapUI* à partir de la racine du projet (avec son extension ``.xml``).

* ``[3]`` : Nom de la Suite de test *SoapUI* contenant le cas de test.

* ``[4]`` : Nom du cas de test à exécuter.

|

Ci-dessous un exemple de fichier de test *SoapUI* et l’automatisation du cas de test |squashTM| associé |_| :

    .. container:: image-container

        .. image:: ../_static/autom/soapui-test-1.png

    .. container:: image-container

        .. image:: ../_static/autom/soapui-test-2.png

|

.. _automate_with_uft:

--------------------

Automatisation avec UFT
---------------------------

Cette fonctionnalité est disponible uniquement dans la version *Premium* de |squashAutom|.

Le prérequis nécessaire pour l'utiliser est l'installation de l'agent *OpenTestFactory* pour l'environnement d'exécution
des tests *UFT*.

1. Référence du test
^^^^^^^^^^^^^^^^^^^^

Pour lier un cas de test |squashTM| à un test automatisé, le champ *Référence* du test automatisé du bloc
*Automatisation* du cas de test doit avoir une forme spécifique au framework de test utilisé |_| :

``[1]``

Avec :

* ``[1]`` : Chemin vers le dossier du test à exécuter.

2. Nature des paramètres Squash TM exploitables
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Voici le tableau des paramètres exploitables |_| :

.. csv-table::
   :header: "Nature","Clé"
   :align: center

   "Nom du jeu de donnée","DSNAME"
   "Paramètre d’un jeu de donnée","DS_[nom]"
   "Référence du cas de test","TC_REF"
   "CUF cas de test","TC_CUF_[code]"
   "CUF itération","IT_CUF_[code]"
   "CUF campagne","CPG_CUF_[code]"
   "CUF suite de tests","TS_CUF_[code]"

*Légende :*

* ``CUF`` : *Custom Field / Champ personnalisé*
* ``[code]`` : *Valeur renseignée dans le champ “Code” d’un CUF*
* ``[nom]`` : *nom du paramètre tel que renseigné dans Squash TM*

3. Utilisation de paramètres Squash TM
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Il est possible lors de l’exécution d’un cas de test |squashTM| automatisé avec *UFT* d’exploiter des
paramètres |squashTM| au sein de celui-ci.

Pour cela, il faut suivre les étapes suivantes |_| :

* Renseigner des champs personnalisés dans |squashTM| et les associer au projet portant le plan de tests à exécuter.

* S'assurer que les champs *code* des paramètres correspondent aux noms des paramètres existants dans le
  test *UFT*.

.. note:: |squashTM| ajoute un préfixe au *code* du champ personnalisé transmis. Assurez-vous de le prendre en compte.
          Voir la `documentation <https://sites.google.com/a/henix.fr/wiki-squash-tm/administrator-guide/05---custom-field-administration>`_
          de |squashTM| pour plus d'information.

|

Ci-dessous un exemple de test *UFT* et l’automatisation du cas de test |squashTM| associé |_| :

    .. container:: image-container

        .. image:: ../_static/autom/uft-test-with-params-1.png

    .. container:: image-container

        .. image:: ../_static/autom/uft-test-with-params-2.png

|

.. _execute_from_squash:

****************************************************
Déclenchement d’un plan d’exécution depuis Squash TM
****************************************************

Déclaration du serveur Squash Orchestrator
==========================================

Afin de lancer un plan d'exécution manuellement depuis |squashTM|, il est nécessaire de déclarer le serveur
|squashOrchestrator| qui exécutera les tests automatisés dans les environnements adaptés.
Cette déclaration se fait dans la section *Serveurs d'exécution automatisée* de l'espace *Administration* |_| :

    .. container:: image-container

        .. image:: ../_static/autom/automation_servers_declaration.png

* ``Nom`` : Le nom du serveur tel qu'il apparaîtra dans l'espace *Cas de test*.

* ``Type`` : Sélectionnez *squashAutom* dans la liste déroulante.

* ``Url`` : L'adresse du service *Receptionist* de |squashOrchestrator|.

.. warning:: Le service *bus d'évènements* de |squashOrchestrator| doit impérativement être accessible à la même url que le service
             *Receptionist* mais sur le port 38368.

Une fois le serveur créé, vous pouvez préciser un token servant pour l'authentification au serveur.

    .. container:: image-container

        .. image:: ../_static/autom/token.png

.. note:: La présence d'un token est obligatoire pour la bonne exécution de tests automatisés depuis |squashTM|.
          Dans le cas où l'authentification au serveur d'automatisation n'est pas requise par ce dernier,
          il faut quand même renseigner une valeur quelconque de token dans |squashTM|


Exécution de la suite automatisée
=================================

L'exécution d'un plan d'exécution automatisée se déroule de la façon habituelle dans |squashTM| :

* Accédez au plan d'exécution de l'itération ou de la suite de test choisie.

* Exécutez les tests automatisés en utilisant un des boutons de l'image ci-dessous :

    .. container:: image-container

        .. image:: ../_static/autom/test-plan.png

* Une popup de suivi d'exécution apparaît.

.. note:: La popup de suivi contient une nouvelle section qui rappelle le nom des tests en cours d'exécution par
          |squashOrchestrator|. Cependant il n'y a pas de suivi d'avancement dans la version actuelle.

|

.. _result_publication:

**********************************************************************
Remontées de résultats après exécution d’un plan d’exécution Squash TM
**********************************************************************

Quel que soit la façon dont le plan d’exécution est déclenché (depuis |squashTM| ou depuis un pipeline *Jenkins*), vous
avez en fin d’exécution une remontée de résultats au sein de |squashTM| qui diffère suivant si vous êtes sous licence
**Squash AUTOM Community** ou **Squash AUTOM Premium**.

--------------------

Squash AUTOM Community
======================

Après exécution d’un plan d’exécution |squashTM| (itération ou suite de tests), les informations suivantes sont mises à
jour :

* Mise à jour du statut des ITPI.

* Mise à jour du statut de la suite automatisée.

* Le rapport au format *Allure* correspondant à l'ensemble des tests exécutés.

* Les rapports d’exécution des différents ITPI sont accessibles depuis l’onglet de consultation des suites automatisées |_| :

.. container:: image-container

    .. image:: ../_static/autom/automated-suite-tab.png

.. note:: Tous les résultats de la suite automatisée sont compilés dans un rapport au format *Allure*, disponible dans
          la liste de rapports sous forme d'archive *.tar*.

          Pour plus d'information sur la façon d'exploiter ce rapport et les possibilités de personnalisation, veuillez
          consulter la `documentation Allure <https://docs.qameta.io/allure/>`_.

Cependant, voici ce qu’il ne se passe pas :

* Création d’une nouvelle exécution pour chaque ITPI qui a été exécuté.

--------------------

Squash AUTOM Premium
====================

Si vous disposez des composants **Squash AUTOM Premium**, vous avez accès à deux types de remontées d’informations |_| :

* Légère (valeur par défaut).

* Complète.

Le choix du type de remontée se fait par projet en accédant à la configuration du plugin **Squash TM Result Publisher**
depuis l’onglet *Plugins* de la page de consultation d’un projet :

    .. container:: image-container

        .. image:: ../_static/autom/plugin-complete-switch.png

|

Remontée d’information Légère
-----------------------------

En choisissant la remontée d’information *Légère*, les informations suivantes sont mises à jour après exécution d’un plan
d’exécution |squashTM| (itération ou suite de tests) |_| :

* Mise à jour du statut des ITPI.

* Mise à jour du statut de la suite automatisée.

* Le rapport au format *Allure* correspondant à l'ensemble des tests exécutés.

* Les rapports d’exécution des différents ITPI sont accessibles depuis l’onglet de consultation des suites automatisées |_| :

.. container:: image-container

    .. image:: ../_static/autom/automated-suite-tab.png

.. note:: Tous les résultats de la suite automatisée sont compilés dans un rapport au format *Allure*, disponible dans
          la liste de rapports sous forme d'archive *.tar*.

          Pour plus d'information sur la façon d'exploiter ce rapport et les possibilités de personnalisation, veuillez
          consulter la `documentation Allure <https://docs.qameta.io/allure/>`_.

Cependant, voici ce qu’il ne se passe pas |_| :

* Création d’une nouvelle exécution pour chaque ITPI qui a été exécuté.

|

Remontée d’information Complète
-------------------------------

En choisissant la remontée d’information *Complète*, les informations suivantes sont mises à jour après exécution d’un
plan d’exécution |squashTM| (itération ou suite de tests) |_| :

* Mise à jour du statut des ITPI.

* Création d’une nouvelle exécution pour chaque ITPI.

* Mise à jour du statut de la suite automatisée.

* Le rapport au format *Allure* correspondant à l'ensemble des tests exécutés.

* Les rapports d’exécution des différentes exécutions sont accessibles depuis l’onglet de consultation des suites
  automatisées ou depuis l’écran de consultation de l’exécution (ils sont présents dans les pièces jointes).

.. container:: image-container

    .. image:: ../_static/autom/iteration-execution-tab.png

.. container:: image-container

    .. image:: ../_static/autom/iteration-execution-detail.png


.. note:: Tous les résultats de la suite automatisée sont compilés dans un rapport au format *Allure*, disponible dans
          la liste de rapports sous forme d'archive *.tar*.

          Pour plus d'information sur la façon d'exploiter ce rapport et les possibilités de personnalisation, veuillez
          consulter la `documentation Allure <https://docs.qameta.io/allure/>`_.

|
