..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2021 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.



####################
Guide d'Installation
####################

.. contents::
   :local:
   :depth: 1

|

************************
Squash Generator Service
************************

Présentation
============

Ce micro-service permet la récupération de plans d'exécution de tests automatisés |squashTM| pour exécution au sein d'un PEaC.

Installation
============

Le micro-service est inclus dans l'image Docker de |squashOrchestrator|.
Pour plus de détails sur l'installation de |squashOrchestrator|, consultez la :ref:`section dédiée<squash_orchestrator_installation>`
de la documentation.

Ce micro-service existe en version |squashDevops| **Community** et |squashDevops| **Premium** et marche conjointement
avec le plugin Test Plan Retriever dans la même version (Community ou Premium).

Pour exécuter l'image Docker de |squashOrchestrator| avec la version |squashDevops| **Premium** du micro-service
**Squash Generator**, le paramètre ``-e SQUASH_LICENCE_TYPE=premium`` est à préciser dans la commande ``docker run`` de
déclenchement de |squashOrchestrator|.

|

*****************************************
Plugin Test Plan Retriever pour Squash TM
*****************************************

Présentation
============

Ce plugin permet la récupération de plans d'exécution de tests automatisées |squashTM| par une instance de |squashOrchestrator|.

Installation
============

Le plugin existe en version **Community** (*squash.tm.rest.test.plan.retriever.community-1.0.0.RELEASE.jar*) librement
téléchargeable ou **Premium** (*squash.tm.rest.test.plan.retriever.premium-1.0.0.RELEASE.jar*) accessible sur demande.

Pour l’installation, merci de vous reporter au protocole d’installation d’un plugin |squashTM|
(https://sites.google.com/a/henix.fr/wiki-squash-tm/installation-and-exploitation-guide/2---installation-of-squash-tm/7---jira-plug-in).

.. warning:: Ce plugin est compatible avec une version *1.22.2.RELEASE* ou supérieure de |squashTM|.

|

*********************************
Plugin Squash DEVOPS pour Jenkins
*********************************

Présentation
============

Ce plugin Jenkins permet d'intégrer facilement des appels à |squashOrchestrator| au sein d'un pipeline.

Installation
============

Le plugin est sous la forme d’un fichier ``.hpi`` (*squash-devops-1.1.0.hpi*) librement téléchargeable depuis
https://www.squashtest.com/community-download.

Pour l’installation, soumettez le plugin depuis l’onglet *Avancé* de l’écran de gestion des plugins de *Jenkins* |_| :

.. container:: image-container

    .. image:: ../_static/devops/squash-devops-plugin-jenkins-upload.png

.. warning:: Ce plugin est compatible avec une version *2.164.1* ou supérieure de *Jenkins*.

|
