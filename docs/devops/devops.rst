..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2021 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

.. _devops:

#############
Squash DEVOPS
#############

.. toctree::
   :hidden:
   :maxdepth: 2

   Installation<install.rst>
   Appel au Squash Orchestrator depuis un pipeline Jenkins<callFromJenkins.rst>
   Récupération d’un plan d’exécution Squash TM depuis un PEAC<callWithPeac.rst>

Ce guide vous présente les possibilités offertes par la version *1.1.0.RELEASE* de |squashDevops|.

Cette version *1.1.0.RELEASE* met à votre disposition les composants suivants |_| :

* **Micro-service Squash TM Generator pour Squash Orchestrator** : il s’agit d’un micro-service de |squashOrchestrator|
  permettant la récupération d’un plan d’exécution |squashTM| au sein d’un PEAC (Plan d’Exécution «as code»).
  Consultez le guide utilisateur de :ref:`Squash AUTOM<autom>` pour plus d’informations sur |squashOrchestrator| et les
  PEAC.

..

* **Plugin Test Plan Retriever pour Squash TM** : ce plugin pour |squashTM| permet l’envoi à |squashOrchestrator| de
  détails sur un plan d’exécution |squashTM|.

..

* **Plugin Squash DEVOPS pour Jenkins** : ce plugin pour *Jenkins* facilite l’envoi d’un PEAC à Squash Orchestrator
  depuis un pipeline *Jenkins*.

|
