..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2021 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.



###########################################################
Récupération d’un plan d’exécution Squash TM depuis un PEAC
###########################################################

.. contents::
   :local:
   :depth: 1

|

|squashDevops| vous donne la possibilité de récupérer un plan d’exécution de tests automatisés définis dans |squashTM|
depuis un PEAC. Ce PEAC pouvant être déclenché depuis un pipeline *Jenkins* par exemple
(voir la :ref:`page correspondante<call_from_jenkins>` de ce guide).

.. note:: Pour le bon fonctionnement de cette fonctionnalité, les plugins Test Plan Retriever et Result Publisher
          doivent être installés sur l'instance Squash TM ciblé.

|

*********
Prérequis
*********

Afin de pouvoir récupérer un plan d’exécution |squashTM| depuis un PEAC, vous avez besoin d’avoir effectué les actions
suivantes dans |squashTM| |_| :


* Création d’un utilisateur appartenant au groupe *Serveur d’automatisation de tests*.

* Création d’un plan d’exécution (itération ou suite de tests) contenant au moins un ITPI lié à un cas de test
  automatisé suivant les instructions du guide utilisateur |squashAutom| (voir :ref:`ici<automate_with_squash>`)

|

***********************************************************************************
Intégration de l’étape de récupération d’un plan d’exécution Squash TM dans un PEAC
***********************************************************************************

Pour récupérer un plan d’exécution |squashTM| dans un PEAC, il faut faire appel à l’action *generator* correspondante.

Voici ci-dessous un exemple simple de PEAC au format *Json* permettant de récupérer un plan d’exécution |squashTM| |_| :

    .. code-block:: bash

        {
            "apiVersion": "opentestfactory.org/v1alpha1",
            "kind": "Workflow",
            "metadata": {
                "name": "Simple Workflow"
            },
            "defaults": {
                "runs-on":"ssh"
            },
            "jobs": {
                "explicitJob": {
                    "runs-on":"ssh",
                    "generator":"tm.squashtest.org/tm.generator@v1",
                    "with": {
                        "testPlanUuid":"1e2ae123-6b67-44b2-b229-274ea17ad489",
                        "testPlanType":"Iteration",
                        "squashTMUrl":"https://mySquashTMInstance.org/squash",
                        "squashTMAutomatedServerLogin":"tfserver",
                        "squashTMAutomatedServerPassword":"tfserver",
                        "technologyLabels":{
                            "ranorex": ["windows","ranorex"],
                            "robotframework": ["linux","robotframework"],
                            "junit": ["linux","junit"]
                        }
                    }
                }
            }
        }

Un step *generator* |squashTM| doit contenir les paramètres suivants |_| :

* ``testPlanType`` : Correspond au type du plan de test à récupérer dans |squashTM|. Seules les valeurs *Iteration* et
  *TestSuite* sont acceptées.

..

* ``testPlanUuid`` : Correspond à l’UUID du plan de test souhaité. Celui-ci peut être récupéré dans le panneau
  Information de l’itération ou de la suite de tests souhaitée dans |squashTM|.

..

* ``squashTMUrl`` : URL du |squashTM| à viser.

..

* ``squashTMAutomatedServerLogin`` : Nom de l’utilisateur du groupe *Serveur d’automatisation de tests* à utiliser dans
  |squashTM|.

..

* ``squashTMAutomatedServerPassword`` : Mot de passe de l’utilisateur du groupe *Serveur d’automatisation de tests* à
  utiliser dans |squashTM|.

..

[*Champs Optionnels*] |_| :

* ``tagLabel`` : Spécifique à la version **Premium** - Correspond au nom du champ personnalisé de type *tag* sur lequel on
  souhaite filtrer les cas de test à récupérer. Il n’est pas possible d’en spécifier plusieurs.

..

* ``tagValue`` : Spécifique à la version **Premium** - Correspond à la valeur du champ personnalisé de type *tag* sur
  lequel on souhaite filtrer les cas de test à récupérer. Il est possible d’indiquer plusieurs valeurs séparées par des
  “|” (*Exemple:* valeur1|valeur2). Il faut au moins l’une des valeurs pour que le cas de test soit pris en compte.

.. warning:: Si l’un des deux champs *tagLabel* ou *tagValue* est présent, alors l’autre champ **doit** également être
             renseigné.

..

* ``technologyLabels`` : A utiliser notamment si vous récupérez un plan d'exécution contenant des tests devant s'exécuter
  sur des environnements différents.

  Il permet de spécifier pour chaque framework d'automatisation les étiquettes de l'environnement d'exécution à cibler.
  Celles-ci seront ajoutées aux étiquettes du runs-on du job Generator.

  Pour une technologie, les étiquettes se précisent en ajoutant une entrée à la liste du champ sous la forme
  ``"préfixe de la technologie": ["etiquette1", "etiquette2"]``

.. note:: Par défaut, les jobs créés par le Generator suite à la récupération d'un plan d'exécution se voient attribués une étiquette
          correspondant au préfixe de la technologie de test (nom de la technologie en minuscule sans espace, par exemple
          "robotframework" pour Robot Framework) en plus de celles renseignées dans le runs-on du job generator.


|

*********************************************************
Paramètres Squash TM exploitables dans un test automatisé
*********************************************************

En exécutant un PEAC avec récupération d’un plan d’exécution |squashTM|, ce dernier transmet différentes informations
sur les ITPI qu’il est possible d’exploiter dans un cas de test *Cucumber*, *Cypress*, ou *Robot Framework*.

Pour plus d'informations veuillez consulter la section :ref:`Exploitation de paramètres Squash TM<squash_params>` de la
documentation de |squashAutom|, ainsi que la section dédiée à l'automatisation du framework de test souhaité.

|

*********************************************************
Remontée d’informations vers Squash TM en fin d’exécution
*********************************************************

La nature de la remontée d’informations à |squashTM| en fin d’exécution d’un plan d’exécutions |squashTM| va dépendre de
si vous êtes sous licence **Squash AUTOM Community** ou **Squash AUTOM Premium**.

Consultez le guide utilisateur |squashAutom| pour plus d'informations (voir :ref:`ici<result_publication>`).

|
