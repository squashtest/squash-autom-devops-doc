..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2021 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.


.. _call_from_jenkins:

#######################################################
Appel au Squash Orchestrator depuis un pipeline Jenkins
#######################################################

.. contents::
   :local:
   :depth: 1

|

***************************************************
Configuration d’un Squash orchestrator dans Jenkins
***************************************************

Pour accéder à l’espace de configuration du |squashOrchestrator|, il faut tout d’abord se rendre dans l’espace
*Configurer le système* du *System Configuration*, accessible par l’onglet *Administrer Jenkins* |_| :

.. container:: image-container

    .. image:: ../_static/devops/jenkins-system-configuration.png

Un panel nommé *Squash Orchestrator servers* sera ensuite disponible |_| :

.. container:: image-container

    .. image:: ../_static/devops/jenkins-squash-orchestrator-server.png

* ``Server id`` : Cet ID est généré automatiquement et ne peut être modifié. Il n’est pas utilisé par l’utilisateur.

..

* ``Server name`` : Ce nom est défini par l’utilisateur. C’est celui qui sera mentionné dans le script du pipeline lors
  d’une exécution de workflow.

..

* ``Receptionist endpoint URL`` : L’adresse du micro-service *receptionist* de l'orchestrateur, avec son port tel que
  défini au lancement de l'orchestrateur.
  Reportez-vous à la documentation de |squashOrchestrator| pour plus de détails.

..

* ``Workflow Status endpoint URL`` : L’adresse du micro-service *observer* de l'orchestrateur, avec son port tel que
  défini au lancement de l'orchestrateur.
  Reportez-vous à la documentation de |squashOrchestrator| pour plus de détails.

..

* ``Credential`` : Credential *Jenkins* de type *Secret text* contenant un *JWT Token* permettant de s’authentifier auprès de l'orchestrateur.
  Reportez-vous à la documentation de |squashOrchestrator| pour plus de détails sur l’accès sécurisé
  à l'orchestrateur.

..

* ``Workflow Status poll interval`` : Ce paramètre correspond au temps de mise à jour du statut du workflow.

..

* ``Workflow creation timeout`` : Timeout sur la réception du PEAC par le *receptionist* côté orchestrateur.

..

|

*******************************************************
Appel au Squash Orchestrator depuis un pipeline Jenkins
*******************************************************

Une fois qu'il y a au moins un |squashOrchestrator| configuré dans *Jenkins*, il est possible de faire appel à
l’orchestrateur depuis un job *Jenkins* de type pipeline grâce à une méthode de pipeline dédiée.

Ci-dessous, un exemple de pipeline simple utilisant la méthode d’appel à l’orchestrateur |_| :

    .. code-block:: bash

        node {
           stage 'Stage 1 : sanity check'
           echo 'OK pipelines work in the test instance'
           stage 'Stage 2 : steps check'
           configFileProvider([configFile(
        fileId: '600492a8-8312-44dc-ac18-b5d6d30857b4',
        targetLocation: 'testWorkflow.json'
        )]) {
           	def workflow_id = runSquashWorkflow(
               	workflowPathName:'testWorkflow.json',
               	workflowTimeout: '20S',
               	serverName:'defaultServer'
           	)
           	echo "We just ran The Squash Orchestrator workflow $workflow_id"
           }
        }

La méthode *runSquashWorkflow* permet de transmettre un PEAC à l'orchestrateur pour exécution.

Elle dispose de 3 paramètres |_| :

* ``workflowPathName`` : Le chemin vers le fichier contenant le PEAC. Dans le cas présent, le fichier est injecté via
  le plugin *Config File Provider*, mais il est également possible de l’obtenir par d’autres moyens (récupération depuis
  un SCM, génération à la volée dans un fichier, ...).

..

* ``workflowTimeout`` : Timeout sur l’exécution des actions. Ce paramètre intervient par exemple si un environnement
  n’est pas joignable (ou n’existe pas), ou si une action n’est pas trouvée par un *actionProvider*. Il est à adapter en
  fonction de la durée d’exécution attendue des différents tests du PEAC.

..

* ``serverName`` : Nom du serveur |squashOrchestrator| à utiliser. Ce nom est celui défini dans l’espace de
  configuration *Squash Orchestrator servers* de *Jenkins*.

|
