..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2021 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

#################################################################
Bienvenue sur la documentation de Squash AUTOM et Squash DEVOPS !
#################################################################

|squashAutom| est un ensemble de composants pour la gestion de l'exécution de vos tests automatisés.

|squashDevops| est un ensemble de composants pour l'intégration de l'exécution de vos tests fonctionnels automatisés à
votre chaîne d'intégration continue.

.. toctree::
   :maxdepth: 2

   Squash AUTOM<autom/autom.rst>
   Squash DEVOPS<devops/devops.rst>
   FAQ<faq/faq.rst>

|
